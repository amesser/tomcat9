#!/bin/sh
#
# SYSVinit script helper to wrap the systemd startup script
#

set -e

# redirect stdio
exec </dev/null
exec >>/var/log/tomcat9/catalina.out
exec 2>&1
# write an initial log entry
echo "[$(date +'%FT%T%z')] starting..."

# make sure Tomcat is started with system locale

# restore LC_ALL that was (un)set at initscript startup
case $saved_LC_ALL in
(x*)	LC_ALL=${saved_LC_ALL#x} ;;
(*)	unset LC_ALL ;;
esac
# read global locale configuration
test -r /etc/default/locale && . /etc/default/locale
# export all POSIX locale-relevant environment variables if set
for v in LANG LC_CTYPE LC_NUMERIC LC_TIME LC_COLLATE LC_MONETARY \
    LC_MESSAGES LC_PAPER LC_NAME LC_ADDRESS LC_TELEPHONE \
    LC_MEASUREMENT LC_IDENTIFICATION LC_ALL; do
	eval "x=\${$v-x}"
	test x"$x" = x"x" || eval export "$v"
done

# hand control to the systemd startup script we wrap
exec /usr/libexec/tomcat9/tomcat-start.sh "$@"
